@extends('layouts.app')

@section('content')
<div class="container" ng-app="userRecords" ng-controller="usersController">
    <div class="row justify-content-center">
        <div class="col-md-12">
            Listing Users
          
          <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Verification Status</th>
                <th scope="col">Added On</th>
              </tr>
            </thead>
            <tbody>
              {{-- @foreach($users as $user) --}}
              <tr ng-repeat="user in users">
                <th>@{{ user.id }}</th>
                <td>@{{ user.name }}</td>
                <td>@{{ user.email }}</td>
                <td>@{{ (user.email_verified_at) ? 'Verified' : 'Pending' }}</td>
                <td>@{{ user.created_at }}</td>
              </tr>
              {{-- @endforeach --}}
            </tbody>
          </table>
        </div>
    </div>
</div>

@push('scripts')
    <!-- ANGULAR -->
    <!-- all angular resources will be loaded from the /public folder -->
    <script src="{{ asset('app/app.js') }}"></script> 
    <script src="{{ asset('app/controllers/users.js') }}"></script>  
@endpush
@endsection